package org.stool.netty.http.download;

public interface Handler<E> {
    void handle(E event);
}
